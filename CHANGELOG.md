# Changelog

All notable changes to this project will be documented in this file.

## [0.1.6] - 2024-07-02

### 🐛 Bug Fixes

- License link

### 🚜 Refactor

- Change xsh to py
- Change xsh to py (#2)

### 📚 Documentation

- Add changelog
- Adjustment in chagelog

## [0.1.5] - 2024-02-25

### 🚀 Features

- Change setuptools to hatch

## [0.1.4] - 2022-09-08

### 🐛 Bug Fixes

- License
- License

<!-- generated by git-cliff -->
